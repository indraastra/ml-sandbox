import math
import numpy as np
from PIL import Image
import sklearn.feature_extraction.image as skimage 
import tensorflow as tf


crop_size = 64
image = Image.open("test_image.png")
np_image = np.array(image)
h, w, d = np_image.shape
tiles_h, tiles_w = math.ceil(h / crop_size), math.ceil(w / crop_size)
#image.show('Original image')
image_batch = np.expand_dims(np_image, 0)

with tf.Session() as sess:
    print('Original images shape:', image_batch.shape)
    patches = tf.extract_image_patches(
            images=image_batch,
            ksizes=[1, crop_size, crop_size, 1], 
            strides=[1, crop_size, crop_size, 1], 
            rates=[1, 1, 1, 1], 
            padding='SAME')
    patches = tf.reshape(patches, [tiles_h, tiles_w, crop_size, crop_size, 3])
    hstack = tf.map_fn(lambda row: tf.concat(tf.unstack(row), axis=1), patches)
    reconstructed_image = tf.concat(tf.unstack(hstack), axis=0)

    patches, reconstructed_image = sess.run([patches, reconstructed_image])

    print('Patches shape:', patches.shape)
    print('Patches type:', patches.dtype)
    print('Reconstructed image shape:', reconstructed_image.shape)
    print('Reconstructed image type:', reconstructed_image.dtype)
    Image.fromarray(reconstructed_image, mode='RGB').show('Reconstructed image')
